# Model View Controller:

*Model ni Database-tai hariltsdag heseg.

* Controller, Model hesguuded bid PHP Class-uudiig bichij ogdog.

* Manai hereglegchid maani controller uruu handaj heregtei medeellee awna. Controller-> View uruu data damjuulna.

*Controller maani heregtei    data-aa Models-s duudaj awna.
*Controller maani View uruu data-aa damjuulna View ni HTML uusgeed Controller maani teriig huleej awaad hereglegchid uruu damjuulna.

# Project-iin medeelel:

* Folders: 
    * Config
    * Controllers
    * Language
    * Libraries/lib
    * logs
    * models
    * views
    * www

# Lib 
 All we need for Framework

Files:
* init.php = ene script ni manai framework-g duudaj ogno.
* router.class.php = class-g dotroo aguulsan bna gsen ug. router gdg ni Url path-s controller action parametr gedg ni yu ym be gedgiig parse hiij ylgaj awah salgaj awah uuregtei.

# config

* config class maani framework-n tohirgoog zohitsuuldag...

# Controller:
 * orders DB-g end uusgene.
 * Controll-g ajillauulah  App class bolon Controller klass, tuunees udamshih PagesController class-g bichne.


 # View:
   * Template hiine (HTML)
   * default HTML ni layout(arga ys ni bolj baina).


  # init.php 
   * end oruulj ogson buh func-uud bna app deer garj irne...

  
  # CMS = content management system..
   * cms maani web huudsuudiig DB-d hadgalj baigaa.Edgeer web huudasuudtai ajilladag model class-uudiig bichne. Eronhiidoo bugded n ashiglagdah model class bichne.

   * Page model gesen class maani zowhon ter pages gesen taple-tai ajillana.WEB-tei ajillahad shaardlagatai func-uudiig bid implement hj ogno.

   * page ni modell-s udamshsan bna. Page Model class-g ashiglaad page-uudiig unshaad web deer  gargaj irne.
